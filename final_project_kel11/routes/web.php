<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\CategoriController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PesananController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//front end
// Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/shop', [HomeController::class, 'shop'])->name('shop');
Route::get('/detail/{produt_id}', [HomeController::class, 'detail'])->name('detail');
Route::get('/categori-home/{categori_id}', [HomeController::class, 'categori'])->name('categori.product');
Route::get('/game/{game_id}',[HomeController::class, 'game'])->name('game.products');
Route::post('/pesanan', [HomeController::class, 'pesan'])->name('pesanan');
Route::get('/cart', [HomeController::class, 'cart'])->name('cart');
Route::get('/checkout', [HomeController::class, 'checkout'])->name('checkout');
Route::post('/payment', [HomeController::class, 'payment'])->name('payment');

Route::middleware(['auth'])->group(function () {
    //route CRUD categori
    Route::get('/categori', [CategoriController::class, 'index'])->name('categori.index');
    Route::get('/categori/create', [CategoriController::class, 'create'])->name('categori.create');
    Route::post('/categori', [CategoriController::class, 'store'])->name('categori.store');
    Route::put('/categori/{categori_id}', [CategoriController::class, 'update'])->name('categori.update');
    Route::delete('/categori/{categori_id}', [CategoriController::class, 'destroy'])->name('categori.destroy');

    //route CRUD Game
    Route::get('/game', [GameController::class, 'index'])->name('game.index');
    Route::get('/game/create', [GameController::class, 'create'])->name('game.create');
    Route::post('/game', [GameController::class, 'store'])->name('game.store');
    Route::put('/game/{game_id}', [GameController::class, 'update'])->name('game.update');
    Route::delete('/game/{game_id}', [GameController::class, 'destroy'])->name('game.destroy');

    //route CRUD Product
    Route::get('/product', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
    Route::post('/product', [ProductController::class, 'store'])->name('product.store');
    Route::get('/product/{product_id}', [ProductController::class, 'show'])->name('product.show');
    Route::get('/product/{product_id}/edit', [ProductController::class, 'edit'])->name('product.edit');
    Route::put('/product/{product_id}', [ProductController::class, 'update'])->name('product.update');
    Route::delete('/product/{product_id}', [ProductController::class, 'destroy'])->name('product.destroy');


    //route untuk tampil pesanan
    Route::get('/pesanan', [PesananController::class, 'index'])->name('pesanan.index');
    Route::get('/invoice/{pesanan_id}', [PesananController::class, 'invoice'])->name('pesanan.invoice');
    Route::get('/invoice/{pesanan_id}/pdf', [PesananController::class, 'invoiceExport'])->name('pesanan.invoice.export');
});
