@extends('data-admin.layout.invoice_master')

@section('title')
Invoice | Dashboard
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Invoice</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="callout callout-info">
            <h5><i class="fas fa-info"></i> Note:</h5>
            This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
          </div>


          <!-- Main content -->
          <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info my-2">
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Invoice #{{$pesanan->kode_unik}}</b><br>
                <br>
                <b>Kode Pemesanan:</b> {{ $pesanan->kode_pesanan }}<br>
                <b>Payment Due:</b> {{ $pesanan->updated_at->format('d / M / Y') }}<br>
                <b>{{ $pesanan->user->username }}</b>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Product</th>
                    <th>Description</th>
                    <th>Subtotal</th>
                  </tr>
                  </thead>
                  <tbody>
                    @forelse ($pesananDetail as $key => $item)
                    <tr>
                        <td>{{ $item->qty }}</td>
                        <td>{{ $item->products->nama }}</td>
                        <td>{{ $item->products->deskripsi }}</td>
                        <td>@currency($item->total_harga)</td>
                      </tr>
                    @empty
                        Data Kosong
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-6">
                <p class="lead">Payment Methods:</p>
                <img src="{{asset('/template/dist/img/credit/visa.png')}}" alt="Visa">
                <img src="{{asset('/template/dist/img/credit/mastercard.png')}}" alt="Mastercard">
                <img src="{{asset('/template/dist/img/credit/american-express.png')}}" alt="American Express">
                <img src="{{asset('/template/dist/img/credit/paypal2.png')}}" alt="Paypal">

                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                  plugg
                  dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
              </div>
              <!-- /.col -->
              <div class="col-6">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <th style="width:50%">Subtotal:</th>
                      <td>@currency($pesanan->total_harga)</td>
                    </tr>
                    <tr>
                      <th>Kode Unik</th>
                      <td>@currency($pesanan->kode_unik)</td>
                    </tr>
                    <tr>
                        @php
                            $jumlah = $pesanan->total_harga + $pesanan->kode_unik
                        @endphp
                      <th>Total:</th>
                      <td>@currency($jumlah)</td>
                    </tr>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-12">
                <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                <a href="{{ route('pesanan.invoice.export', $pesanan->id) }}" target="_blank" class="btn btn-primary float-right" style="margin-right: 5px;"><i class="fas fa-download"></i> Generate PDF</a>
              </div>
            </div>
          </div>
          <!-- /.invoice -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

@endsection
