@extends('data-admin.layout.master')

@section('title')
Dashboard | Halaman Pesanan
@endsection

@section('judul')
Pesanan
@endsection

@push('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Kode Pesanan</th>
          <th>Total Harga</th>
          <th>Payment</th>
          <th>Pembeli</th>
          <th>Tools</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($pesanan as $key => $item)
            @if ($item->status == 1)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->kode_pesanan }}</td>
                <td>@currency($item->total_harga)</td>
                <td>{{ $item->payment }}</td>
                <td>{{ $item->user->username }}</td>
                <td>
                    <a href="{{ route('pesanan.invoice', $item->id) }}" class="btn btn-info btn-sm">Show</a>
                </td>
            </tr>
            @endif
            @empty
            <h2>Data Kosong</h2>
            @endforelse
        </tbody>
    </table>
@endsection

