@extends('data-admin.layout.master')

@section('judul')
Categori
@endsection

@section('title')
Dashboard | Categori
@endsection

@push('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')

<a href="{{ route('categori.create') }}" class="btn btn-primary btn-sm my-3">Tambah Data</a>

<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Nama Categori</th>
      <th>Tools</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($categori as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>
                {{-- <a href="" class="btn btn-warning btn-sm">Edit</a> --}}
                {{-- <form action="" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form> --}}
                <button type="button" class="btn btn-warning text-white" data-toggle="modal" data-target="#updatemodal{{ $item->id }}">
                    update
                </button>
                <button type="button" class="btn btn-danger text-white" data-toggle="modal" data-target="#deletemodal{{ $item->id }}">
                    Delete
                </button>

            </td>
        </tr>
        @empty
        <h2>Data Kosong</h2>
        @endforelse
    </tbody>
</table>
@endsection

@push('modal')
    <!-- Modal Delete-->
    <?php foreach($categori as $key => $item):?>
    <div class="modal fade" id="deletemodal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Yakin Delete Data ini? {{ $item->nama }}, data Akan Dihapus Permanen
        </div>
        <div class="modal-footer">
            <form action="{{ route('categori.destroy', $item->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </div>
        </div>
    </div>
    </div>
    <?php endforeach;?>

    <!-- Modal update-->
    <?php foreach($categori as $key => $item):?>
    <div class="modal fade" id="updatemodal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Confirm Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('categori.update', $item->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="title">Nama Categori</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan nama Cast" value="{{ old("nama", $item->nama) }}">
                    @error('nama')
                        <div class="alert alert-danger my-3">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-warning">Update</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    <?php endforeach;?>

    @include('sweetalert::alert')
@endpush
