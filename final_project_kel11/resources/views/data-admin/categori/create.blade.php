@extends('data-admin.layout.master')

@section('title')
Dasboard | Create Data Categori
@endsection

@section('judul')
Halaman Tambah Data Categori
@endsection

@section('content')
<div>
        <form action="{{ route('categori.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Nama Categori</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="title" placeholder="Masukkan nama Categori" value="{{ old("nama") }}">
                @error('nama')
                    <div id="validationServer03Feedback" class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection


