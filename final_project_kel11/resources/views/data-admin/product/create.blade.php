@extends('data-admin.layout.master')

@section('title')
Product | Create Data Product
@endsection

@section('judul')
Halaman Tambah Data Product
@endsection

@section('content')
<div>
        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Nama Product</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan nama Product" value="{{ old("nama") }}">
                @error('nama')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Harga product</label>
                <input type="text" class="form-control" name="harga" id="body" placeholder="Masukkan harga Product" value="{{ old("harga") }}">
                @error('harga')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="genre">Ready</label>
                <select name="is_ready" id="" class="form-control">
                    <option value="">---------Choise----------</option>
                    <option value="1" {{(old('is_ready') == 1 ) ? ' selected' : ''}}>Product Tersedia</option>
                    <option value="0" {{(old('is_ready') == 0) ? ' selected' : ''}}>Product Tidak Tersedia</option>
                </select>
                @error('is_ready')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="game_id">Game</label>
                <select name="game_id" id="" class="form-control">
                    <option value="">---------Choise----------</option>
                    @forelse ($game as $item)
                    <option value="{{$item->id}}">{{$item->id}} | {{$item->nama}}</option>
                    @empty
                    Tidak Terdapat Game
                    @endforelse
                </select>
                @error('game_id')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="categori_id">Categori</label>
                <select name="categori_id" id="" class="form-control">
                    <option value="">---------Choise----------</option>
                    @forelse ($categori as $item)
                    <option value="{{$item->id}}">{{$item->id}} | {{$item->nama}}</option>
                    @empty
                    Tidak Terdapat categori
                    @endforelse
                </select>
                @error('categori_id')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Deskripsi Product</label>
                <textarea name="deskripsi" id="" cols="30" class="form-control" rows="10">{{ old("deskripsi") }}</textarea>
                @error('deskripsi')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <input type="file" name="gambar">
                @error('gambar')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection


