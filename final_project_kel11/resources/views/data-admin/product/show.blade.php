@extends('data-admin.layout.master')

@section('title')
Example | Show Data
@endsection

@section('judul')
Halaman Show Data
@endsection

@section('content')
<div class="row">
    <div class="col-6">
        <img src="{{ asset('/img/'.$product->gambar) }}" alt="" height="100px" width="100px">
    </div>
    <div class="col-6">
        <h2>{{ $product->nama }} | @currency($product->harga)</h2>
        <p class="card-text">{{ $product->game->nama }} | {{ $product->categori->nama }}</p>
        @if ($product->is_ready == 0)
            <span class="badge badge-danger">Product Tidak Tersedia Sekarang</span>
        @else
            <span class="badge badge-primary">Product Tersedia Sekarang</span>
        @endif
        <br>
        <p>{{ $product->deskripsi }}</p>
    </div>
</div>




<a href="{{ route('product.index') }}" class="btn btn-primary">Kembali</a>

@endsection

