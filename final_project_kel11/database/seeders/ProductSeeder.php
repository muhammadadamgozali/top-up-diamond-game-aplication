<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('product')->insert([
        	'nama' => '300+30 Genesis Crystal',
        	'harga' => '75000',
        	'is_ready' => 1,
        	'gambar' => '1662330994-300-genesis-crystal.png',
        	'deskripsi' => '300 Genesis Crystal dan Bonus 30 Gensis Crystal Akan Masuk Kek Dalam Akun Kamu',
            'categori_id' => 3,
            'game_id' => 1,
        ]);

        DB::table('product')->insert([
        	'nama' => '60 Gensis Crystal',
        	'harga' => '15000',
        	'is_ready' => 1,
        	'gambar' => '1662331179-60-gensis-crystal.png',
        	'deskripsi' => '60 Geneshis crystall akan masuk ke akun genshin impact kamu lho!',
            'categori_id' => 3,
            'game_id' => 1,
        ]);

        DB::table('product')->insert([
        	'nama' => '980+110 Genesis Crystal',
        	'harga' => '245000',
        	'is_ready' => 1,
        	'gambar' => '1662331628-980110-genesis-crystal.png',
        	'deskripsi' => '980 Genesis Crystal dan Bonus 110 Genesis Crystal Akan Masuk Kek Dalam Akun Kamu',
            'categori_id' => 3,
            'game_id' => 1,
        ]);

        DB::table('product')->insert([
        	'nama' => 'Blessing of the Welkin Moon',
        	'harga' => '70000',
        	'is_ready' => 1,
        	'gambar' => '1662331721-blessing-of-the-welkin-moon.png',
        	'deskripsi' => 'Blessing of the Welkin Moon Akan Masuk Kek Dalam Akun Kamu',
            'categori_id' => 3,
            'game_id' => 1,
        ]);
    }
}
