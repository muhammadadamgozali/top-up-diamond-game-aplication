<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'username' => '@rakhaagus33',
            'email' => 'rakhaagus22@gmail.com',
            'password' => Hash::make('farit142'),
            'profil_id' => 1,
        ]);
        DB::table('users')->insert([
            'username' => '@gatrfran31',
            'email' => 'gatfran223@gmail.com',
            'password' => Hash::make('123456'),
            'profil_id' => 2,
        ]);
        DB::table('users')->insert([
            'username' => '@muhammadadamgozali42',
            'email' => 'muhamadgozali@gmail.com',
            'password' => Hash::make('123456'),
            'profil_id' => 3,
        ]);
    }
}
