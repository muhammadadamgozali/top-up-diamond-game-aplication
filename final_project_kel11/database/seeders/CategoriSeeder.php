<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categori')->insert([
        	'nama' => 'Vocer',
        ]);

        DB::table('categori')->insert([
        	'nama' => 'Items',
        ]);

        DB::table('categori')->insert([
        	'nama' => 'Top Up',
        ]);
    }
}
