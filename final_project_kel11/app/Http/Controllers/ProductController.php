<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categori;
use App\Models\Game;
use App\Models\Products;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
Use Alert;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product = Products::all();
        return view('data-admin.product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categori = Categori::all();
        $game = Game::all();
        return view('data-admin.product.create', compact('game', 'categori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $message = [
            'nama.required' => 'Nama Product Tidak Boleh Kosong',
            'harga.required' => 'Harga Product Tidak Boleh Kosong',
            'harga.numeric' => 'Harga Hanya Menerima Inputan Number',
            'is_ready.required' => 'Field Is_ready Tidak Boleh Kosong',
            'game_id.required' => 'Field Game Tidak Boleh Kosong',
            'categori_id.required' => 'Field Categori Tidak Boleh Kosong',
            'gambar.required' => 'Field Gambar Harus Diisi',
            'gambar.mimes' => 'format yang diterima hanya JPG, JPEG, PNG',
            'gambar.max' => 'Ukuran Yang Diboleh Tidak Boleh Melebihi 2Mb',
            'desktripsi.required' => 'Deskripsi Harus Ada'
        ];

        $request->validate([
            'nama' => 'required|min:2',
            'harga' => 'required|numeric',
            'is_ready' => 'required|max:1',
            'categori_id' => 'required',
            'game_id' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:png,jpg,jpeg|max:3048'
        ], $message);

        $namaFile = time().'-'.Str::slug($request->nama, '-').'.'.$request->gambar->extension();

        $request->gambar->move(public_path('img'), $namaFile);

        $product = new Products();

        $product->nama = $request->nama;
        $product->harga = $request->harga;
        $product->is_ready = $request->is_ready;
        $product->categori_id = $request->categori_id;
        $product->game_id = $request->game_id;
        $product->gambar = $namaFile;
        $product->deskripsi = $request->deskripsi;

        $product->save();

        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Products::find($id);
        return view('data-admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = Products::find($id);
        $game = Game::all();
        $categori = Categori::all();
        return view('data-admin.product.edit', compact('product', 'game', 'categori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $message = [
            'nama.required' => 'Nama Product Tidak Boleh Kosong',
            'harga.required' => 'Harga Product Tidak Boleh Kosong',
            'harga.numeric' => 'Harga Hanya Menerima Inputan Number',
            'is_ready.required' => 'Field Is_ready Tidak Boleh Kosong',
            'game_id.required' => 'Field Game Tidak Boleh Kosong',
            'categori_id.required' => 'Field Categori Tidak Boleh Kosong',
            'gambar.required' => 'Field Gambar Harus Diisi',
            'gambar.mimes' => 'format yang diterima hanya JPG, JPEG, PNG',
            'gambar.max' => 'Ukuran Yang Diboleh Tidak Boleh Melebihi 2Mb',
            'deskripsi.required' => 'Field Ini Tidak Boleh Kosong'
        ];

        $request->validate([
            'nama' => 'required|min:2',
            'harga' => 'required|numeric',
            'is_ready' => 'required|max:1',
            'categori_id' => 'required',
            'game_id' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:png,jpg,jpeg|max:3048'
        ], $message);

        $product = Products::find($id);

        if($request->has('gambar')){
            $path = 'img/';
            File::delete($path. $product->gambar);

            $namaFile = time().'-'.Str::slug($request->nama, '-').'.'.$request->gambar->extension();
            $request->gambar->move(public_path('img'), $namaFile);

            $product->gambar = $namaFile;
            $product->save();
        }

        $product->nama = $request->nama;
        $product->harga = $request->harga;
        $product->is_ready = $request->is_ready;
        $product->categori_id = $request->categori_id;
        $product->game_id = $request->game_id;
        $product->deskripsi = $request->deskripsi;
        $product->save();

        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $products = Products::find($id);

        $path = 'img/';
        File::delete($path. $products->gambar);

        $products->delete();

        return redirect('/product');
    }
}
