<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categori;
use App\Models\Game;
use App\Models\Products;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        $product = Products::all();
        $categori = Categori::all();
        $game = Game::all();
        $trending = Products::take(8)->get();

        return view('landing.index', compact('categori', 'game', 'product', 'trending', 'jumlah'));
    }

    public function shop(){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        $product = Products::all();
        $categori = Categori::all();
        $game = Game::all();
        return view('landing.shop', compact('product', 'game', 'categori', 'jumlah'));
    }

    public function categori($id){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        $categori = Categori::find($id);
        $product = Products::all();
        $categoriAll = Categori::all();
        return view('landing.categori', compact('product', 'categori', 'categoriAll', 'jumlah'));
    }

    public function detail($id){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        $product = Products::find($id);
        $categori = Categori::all();
        $game = Game::all();
        return view('landing.detail', compact('product', 'categori', 'game', 'jumlah'));
    }

    public function game($id){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        $game = Game::find($id);
        $product = Products::all();
        $categoriAll = Categori::all();
        return view('landing.game', compact('product', 'game', 'categoriAll', 'jumlah'));
    }

    public function pesan(Request $request){
        if(!Auth::user()){
            return redirect('/login');
        }

        $request->validate([
            'qty' => 'required'
        ]);

        $product = Products::find($request->product);

        $totalHarga = $request->qty * $product->harga;

        $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status', 0)->first();

        if(empty($pesanan)){
            Pesanan::create([
                'users_id' => Auth::user()->id,
                'total_harga' => $totalHarga,
                'status' => 0,
                'kode_unik' => mt_rand(100, 999)
            ]);

            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status', 0)->first();
            $pesanan->kode_pesanan = 'F1-'.$pesanan->id;
            $pesanan->update();
        }else{
            $pesanan->total_harga = $pesanan->total_harga+$totalHarga;
            $pesanan->update();
        }

        PesananDetail::create([
            'qty' => $request->qty,
            'total_harga' => $totalHarga,
            'pesanan_id' => $pesanan->id,
            'product_id' => $request->product
        ]);

        return redirect('/');
    }

    public function cart(){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan)
            {
                $pesanan_details = PesananDetail::where('pesanan_id', $pesanan->id)->get();
            }
        }

        $product = Products::all();
        $categori = Categori::all();
        $game = Game::all();

        return view('landing.cart', compact('jumlah', 'categori', 'pesanan', 'pesanan_details'));
    }

    public function checkout(){

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan) {
                $jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            }else {
                $jumlah = 0;
            }
        }else{
            $jumlah = 0;
        }

        if(Auth::user()) {
            $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status',0)->first();
            if($pesanan)
            {
                $pesanan_details = PesananDetail::where('pesanan_id', $pesanan->id)->get();
            }
        }

        $categoriAll = Categori::all();
        return view('landing.checkout', compact('pesanan', 'categoriAll', 'jumlah', 'pesanan_details', 'pesanan'));
    }

    public function payment(Request $request){
        $request->validate([
            'payment' => 'required'
        ]);

        $pesanan = Pesanan::where('users_id', Auth::user()->id)->where('status', 0)->first();
        $pesanan->status = 1;
        $pesanan->payment = $request->payment;
        $pesanan->update();

        return redirect('/');
    }
}
