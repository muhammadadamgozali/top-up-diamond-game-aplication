<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categori;
use App\Models\Game;
use App\Models\Products;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf;

class PesananController extends Controller
{
    //

    public function index(){
        $pesanan = Pesanan::all();

        return view('data-admin.pesanan.index', compact('pesanan'));
    }

    public function invoice($id){
        $pesanan = Pesanan::find($id);
        $pesananDetail = PesananDetail::where('pesanan_id', $pesanan->id)->get();

        return view('data-admin.pesanan.invoice', compact('pesanan', 'pesananDetail'));
    }

    public function invoiceExport($id){
        $pesanan = Pesanan::find($id);
        $pesananDetail = PesananDetail::where('pesanan_id', $pesanan->id)->get();

        view()->share('pesanan', $pesanan);
        view()->share('pesananDetail', $pesananDetail);
        $pdf = Pdf::loadView('data-admin.pesanan.pdf')->setOptions(['defaultFont' => 'sans-serif']); ;
        $namaFile = $pesanan->kode_pesanan;
        return $pdf->download(time()."_".$namaFile.'.pdf');
    }
}
