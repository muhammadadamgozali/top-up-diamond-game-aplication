<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $table = 'product';
    protected $fillable = ['nama', 'harga', 'is_redy', 'gambar', 'deskripsi', 'categori_id', 'game_id'];

    public function game(){
        return $this->belongsTo(Game::class, 'game_id');
    }

    public function categori(){
        return $this->belongsTo(Categori::class, 'categori_id');
    }

    public function pesanan_details(){
        return $this->hasMany(PesananDetail::Class, 'product_id');
    }
}
