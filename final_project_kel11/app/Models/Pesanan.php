<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    use HasFactory;

    protected $table = 'pesanan';
    protected $fillable = ['status', 'kode_pesanan', 'total_harga', 'kode_unik', 'payment', 'users_id'];

    public function pesanan_details(){
        return $this->hasMany(PesananDetail::Class, 'pesanan_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'users_id');
    }
}
