<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categori extends Model
{
    use HasFactory;

    protected $table = 'categori';
    protected $fillable = ['nama'];

    public function product(){
        return $this->hasMany(Product::class, 'categori_id');
    }
}
